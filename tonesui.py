#!/usr/bin/python

# This is a wrapper around tones(1) from the siggen package.
# it's written in Python Tk
# Copyright raphael.droz@gmail.com
# Licence: GNU General Public Licence
# november 2010
#
# TODO:
# - per-Signal_Frame waveform (comma-separated list)
# - disable_scale_var() doesn't work
# - -c CHANNELS may setup a higher value than needed in some cases
# - add more setting and (really) implement an "advanced settings" interface
# - why does X becomes slow (does tkinter leaks ??)
# - why does the sound starts to be noisy after some SIGALRM ?
# - poll for tones exits according to repeat and duration rather than a static constant (BUTTON_RESTORE_TIMER)

# INSTALL:
# apt-get install tones python-tk
# activate /dev/dsp (snd_pcm_oss module)

from threading import Timer
from sys import exit, argv
import os, signal

try:
    from Tkinter import *
except ImportError:
    print os.path.basename(argv[0]), "needs python-tk (usually known as Tkinter) to work"
    exit(1)

waveforms_available = ["sine", "cosine", "square", "triangle", "sawtooth", "pulse", "noise"]
# the number of seconds the program waits before polling again the last tones process PID
BUTTON_RESTORE_TIMER = 2

# the Frame displaying options for 1 frequency (+ gain, duration, simultaneousness)
# "note" as "e3' are not supported
class Signal_Frame(Frame):
    frequency = None
    freq_text = None
    freq_scale = None

    # because we keep settings if the number of frequencies used is reduced:
    # we do not destroy() the widget, only pack_forget()
    # this param would disappear if I find a Tk kind of is_packed()/is_displayed()
    shown = True

    display_gain = False
    gain = None

    display_duration = False
    duration = None

    # does this tones has to be played simultaneously with the previous
    # if yes, both form a group (comma-separated)
    display_linked = False
    linked = None

    def hide(self):
        self.shown = False
        self.pack_forget()

    def show(self):
        self.shown = True
        self.pack()

    def disable_scale_var(self, event):
        b = None
        self.freq_scale.config(variable=b)
        print "disabled scale", self.freq_scale.cget('variable'), self.freq_scale.cget('length')

    def enable_scale_var(self, event):
        self.freq_scale.config(variable=self.frequency)
        print "enabled scale", self.freq_scale.cget('variable'), self.freq_scale.cget('length')

    @staticmethod
    def last_of_group(group, index):
        try:
            last_of_group = (group[index+1].linked.get() == False or group[index+1].shown == False)
        except IndexError:
            last_of_group = True
        return last_of_group

    def __init__(self, parent=None, with_gain=False, with_duration=False, with_link=False):
        Frame.__init__(self, parent)
        self.frequency = StringVar()
        self.frequency.set(440)

        # additionnal fields
        self.display_gain = with_gain
        self.display_duration = with_duration
        self.display_linked = with_link

        self.gain = IntVar()
        self.duration = IntVar()
        self.linked = IntVar()
        self.linked.set(True)


        # frequency : label, text and scale
        subframe = Frame(self)
        label1 = Label(subframe, text='Enter a numeric value for frequency:', width=28)
        self.freq_text = Entry(subframe, bg='yellow', textvariable=self.frequency)
        self.freq_scale = Scale(subframe, from_=100, to=8000, resolution=1, showvalue=0, takefocus=True,
                                orient=HORIZONTAL, length=320, variable=self.frequency)

        label1.pack(side=TOP, fill='x')
        self.freq_text.pack(side=LEFT)
        self.freq_scale.pack(side=RIGHT, fill='x')

        # pack the whole
        subframe.pack()

        # duration
        if self.display_duration == True:
            subframe_duration = Frame(self)
            # duration
            duration_label = Label(subframe_duration, text='Specific duration')
            duration_label.pack(side=TOP)
            duration_widget = Entry(subframe_duration, textvariable=self.duration, width=5)
            duration_widget.pack(side=TOP)
            # pack it
            subframe_duration.pack(side=LEFT, anchor=CENTER)

        # gain
        if self.display_gain == True:
            subframe_gain = Frame(self)
            label2 = Label(subframe_gain, text='Gain')
            label2.pack(side=TOP)
            gain_entry = Entry(subframe_gain, textvariable=self.gain, width=5)
            gain_entry.pack(side=TOP)
            # pack it
            subframe_gain.pack(side=LEFT)

        # link
        if self.display_linked == True:            
            linked_chk = Checkbutton(self, text="Link with previous", variable=self.linked)
            # pack it
            linked_chk.pack(side=LEFT, anchor=E)

        # self.freq_text.bind("<Return>", func=self.setfocus2)
        self.freq_text.focus()
        self.freq_text.bind("<FocusIn>", func=self.disable_scale_var)
        self.freq_text.bind("<FocusOut>", func=self.enable_scale_var)
        self.show()


# the frame containing several of the above frames,
# basically an array of Signal_Frame which extends a Frame itself
# It especially handle:
#  - adding (= show() if an hidden element is available, or instanciate otherwise)
#  - removing (= pack_forget())
# ... the Signal_Frame's part of the signal_frames list
class Signal_Frames_Frame(Frame):
    number_of_signal = None
    signal_frames = []

    def regen(self, newval):
        self.number_of_signal = newval
        i = 0
        while i < len(self.signal_frames):
            x = self.signal_frames[i]
            if not x.shown and i < self.number_of_signal:
                x.show()
            elif x.shown and i >= self.number_of_signal:
                x.hide()
            i += 1
        while i < self.number_of_signal:
            gain_available = (i > 0)
            duration_available = True

            self.signal_frames.append( Signal_Frame( self, with_gain=gain_available, with_duration=duration_available, with_link=gain_available ) )
            i += 1

    def repack(self, pos=TOP):
        self.pack()
        for i, frame in enumerate(self.signal_frames):
            if not frame.shown:
                break
            if not Signal_Frame.last_of_group(self.signal_frames, i):
                frame.display_duration = False

            frame.pack()
        self.pack(side=pos)

    def max_channels(self):
        i = amax = cur = 1
        while i < len(self.signal_frames):
            x = self.signal_frames[i]
            if not x.shown:
                break
            if not x.linked.get():
                if cur > amax:
                    amax = cur
                    cur = 0
            cur += 1
            i += 1

        return max(cur, amax)

    def __init__(self, parent=None):
        Frame.__init__(self, parent)
        self.regen(1)

# the main class with the unique options for tones

# run() and stop() use a Timer (= signal.alarm())
# That's why we grab SIGALRM in __init__
# The only use of this Timer is to automatically restore
# the "Start/Stop" button status when tones exited (esthetic)
class App:
    setup_level = None
    signal_frames_frame = None
    dobutton = None

    signal_repeat = None
    signal_time = None
    waveform = None

    # pid of the last 'tones' command spawned
    pid = None
    timer = None

    def stop(self):
        if self.pid != None:
            try:
                os.kill(self.pid, signal.SIGTERM)
                self.pid = None
            except OSError:
                self.pid = None
        # stops alarm
        signal.alarm(0)

        self.dobutton.config(command=self.run)
        self.dobutton.config(text="Run")

    # builds the command line from all options
    # spawns it
    # store the pid and runs the alarm()
    def run(self):
        self.dobutton.config(command=self.stop)
        self.dobutton.config(text="Stop")
        cmdline = []
        cmdline.append("tones")

        wave = waveforms_available[ int(self.waveform.curselection()[0]) ]

        # how many channels do we need
        cmdline.append("-c")
        cmdline.append(str(self.signal_frames_frame.max_channels()))

        # infinite
        infinite = False
        if self.signal_repeatinf.get() == True:
            cmdline.append("-l")
            # waveform
            cmdline.append( wave )
            infinite = True

        # repeating
        if self.signal_repeat.get() > 0 and not infinite:
            cmdline.append("-loop")
            cmdline.append( str(self.signal_repeat.get()) )

        if not infinite:
            # waveform
            cmdline.append( wave )
        # duration
        if self.signal_time.get() > 0 and not infinite:
            cmdline.append( ':' + str(self.signal_time.get()) )
        else:
            self.stop()
            return

        all_frames = self.signal_frames_frame.signal_frames
        freq_line = []

        # for each frame, we build the frequency parameter
        for i, frame in enumerate(all_frames):
            if not frame.shown:
                break

            # if true, the next frequency will be a separate command line argument
            # otherwise, it will just be comma-separated
            last_of_group = Signal_Frame.last_of_group(all_frames, i)

            freq_int = int(float(frame.frequency.get()))
            gain_int = frame.gain.get()
            duration_int = frame.duration.get()
            # init string for this tone with the frequency
            astr = str(freq_int)
            if gain_int != None and gain_int != 0 and gain_int != '':
                if(gain_int > 0):
                    astr += "@+" + str(gain_int)
                else:
                    astr += "@-" + str(gain_int)
            # if this frame the last of a group
            if duration_int != None and duration_int != 0 and duration_int != '' and last_of_group:
                astr += ":" + str(duration_int)

            freq_line.append(astr)

            # appends to the global command line and flush
            if last_of_group:
                cmdline.append(",".join(freq_line))
                freq_line = []

        cmdline.append(",".join(freq_line))
        self.pid = os.spawnvp(os.P_NOWAIT, 'tones', cmdline)
        print "run:", 'tones', " ".join(cmdline),"\n", "pid", self.pid
        signal.alarm(BUTTON_RESTORE_TIMER)


    # check if an instance (we created) is still running
    def poll_for_die(self, a, b):
        if self.pid == None:
            self.stop()
            return

        print "got VTALARM", a, "polling for pid", self.pid, 
        dead = os.waitpid(self.pid, os.WNOHANG)[0]
        if dead == 0:
            print "still here, re-alarm in %d sec" % BUTTON_RESTORE_TIMER
            signal.alarm(BUTTON_RESTORE_TIMER)
        else:
            print "gone"
            self.stop()

    def change_frame_number(self, newval):
        self.signal_frames_frame.regen(int(newval))
        self.signal_frames_frame.repack()

    def __init__(self):
        self.setup_level = StringVar()
        self.setup_level.set("D")
        self.signal_repeat = IntVar()
        self.signal_repeat.set(0)
        self.signal_repeatinf = BooleanVar()
        self.signal_repeatinf.set(False)
        self.signal_time = IntVar()
        self.signal_time.set(1000)

        # this instance react to SIGVTALRM... (see the following in run())
        signal.signal(signal.SIGALRM, self.poll_for_die)

        main_frame = Frame(master)

        freq_scale = Scale(main_frame, from_=1, to=8, resolution=1, showvalue=1, 
                           label="Number of signals", orient=HORIZONTAL, length=320,
                           command=self.change_frame_number)

        # waveform
        label_lb = Label(main_frame, text="Waveform")
        self.waveform = Listbox(main_frame, selectmode=SINGLE, exportselection=0, height=len(waveforms_available))
        for item in waveforms_available:
            self.waveform.insert(END, item)
        self.waveform.selection_set(0)

        # other options
        repeat_label = Label(main_frame, text='Repeat:')
        repeat_entry = Entry(main_frame, textvariable=self.signal_repeat, width=5)

        repeatinf_entry = Checkbutton(main_frame, text="Repeat infiniteously", variable=self.signal_repeatinf)

        time_label = Label(main_frame, text='Duration:')
        time_entry = Entry(main_frame, textvariable=self.signal_time, width=5)

        rb_b1 = Radiobutton(main_frame, text="Default", variable=self.setup_level, value="D")
        rb_b2 = Radiobutton(main_frame, text="Advanced", variable=self.setup_level, value="A")

        freq_scale.pack(side=TOP)
        label_lb.pack(side=TOP)
        self.waveform.pack(side=TOP);
        repeat_label.pack(side=TOP)
        repeat_entry.pack(side=TOP)
        repeatinf_entry.pack(side=TOP)
        time_label.pack(side=TOP)
        time_entry.pack(side=TOP)
        rb_b1.pack(side=TOP)
        rb_b2.pack(side=TOP)

        self.dobutton = Button(main_frame, text='Run', command=self.run)
        self.dobutton.pack(side=TOP)

        main_frame.pack(side=LEFT, anchor=NW)

        # the component to redraw which contains every per-signal frames
        self.signal_frames_frame = Signal_Frames_Frame(master)
        # draws every signal frame
        self.signal_frames_frame.repack(RIGHT)

master = Tk()
master.title("Tones Tk")
app = App()
master.mainloop()
